extern crate rand;
extern crate rayon;

use rand::distributions::{IndependentSample, Range};
use rayon::prelude::*;

const DAYS: usize = 365;
const N: u32 = 4;
const DELTA: usize = 7;
const TOL: f64 = 1e-10;
const TOL_REP: u32 = 20;
const BATCH_SIZE: usize = 100000;

fn main() {
    let mut sum: u64 = 0;
    let mut n: u64 = 0;
    let mut res: f64 = 0.;
    let mut last;
    let mut c = 0;
    while c < TOL_REP {
        last = res;
        sum += vec![1; BATCH_SIZE].par_iter().map(|_| get_sample()).sum();
        n += BATCH_SIZE as u64;
        res = (sum as f64) / (n as f64);
        if (res - last).abs() < TOL {
            c += 1;
        }
        println!("{:07}: {:.9}", n, res);
    }
    println!("{:07}: {:.9}", n, res);
}

fn get_sample() -> u64 {
    let range = Range::new(0, DAYS);
    let mut rng = rand::thread_rng();
    let mut birthdays = [0; DAYS];

    let mut res = 0;
    while !test_list(birthdays) {
        birthdays[range.ind_sample(&mut rng)] += 1;
        res += 1;
    }

    res
}

fn test_list(list: [u32; DAYS]) -> bool {
    let mut l = list.to_vec();
    let mut res = l.clone();
    for _ in 0..DELTA {
        if let Some(x) = l.pop() {
            l.insert(0, x);
        }
        res = res.iter().zip(l.iter()).map(|(a, b)| a + b).collect();
    }
    if res.iter().any(|&x| x >= N) {
        true
    } else {
        false
    }
}
